package main

import "os"

const (
	defaultAPIAddr = ":80"
)

type config struct {
	APIAddr string
}

func newConfig() *config {
	cfg := &config{
		APIAddr: defaultAPIAddr,
	}

	if apiAddr := os.Getenv("API_ADDR"); apiAddr != "" {
		cfg.APIAddr = apiAddr
	}

	return cfg
}
