package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type api struct {
	srv    *http.Server
	d      *dict
	logger *log.Logger
}

func newAPI(addr string, d *dict, logger *log.Logger) *api {
	a := &api{
		d:      d,
		logger: logger,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/load", a.load)
	mux.HandleFunc("/get", a.get)
	a.srv = &http.Server{
		Addr:    addr,
		Handler: mux,
	}

	return a
}

func (a *api) run(ctx context.Context) error {
	go func() {
		<-ctx.Done()
		a.srv.Close()
	}()

	if err := a.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

func (a *api) load(w http.ResponseWriter, r *http.Request) {
	values := []string{}
	err := json.NewDecoder(r.Body).Decode(&values)
	if err != nil {
		a.logger.Println("deconding body failed", err)
		http.Error(w, "body must contains array of strings", http.StatusBadRequest)
		return
	}

	a.d.load(values)
}

func (a *api) get(w http.ResponseWriter, r *http.Request) {
	word := r.URL.Query().Get("word")
	if word == "" {
		http.Error(w, "word is required", http.StatusBadRequest)
		return
	}

	anagrams := a.d.getAnagrams(word)

	marshaledResult, err := json.Marshal(anagrams)
	if err != nil {
		a.logger.Println("marshaling response data failed", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	fmt.Fprintln(w, string(marshaledResult))
}
