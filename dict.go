package main

import (
	"sort"
	"strings"
)

type dict struct {
	values map[string][]string
}

func newDict() *dict {
	return &dict{
		values: map[string][]string{},
	}
}

func (d *dict) load(vals []string) {
	values := map[string][]string{}

	for _, v := range vals {
		key := d.prepareKey(v)
		values[key] = append(values[key], v)
	}

	d.values = values
}

func (d *dict) getAnagrams(val string) []string {
	return d.values[d.prepareKey(val)]
}

func (d *dict) prepareKey(val string) string {
	// можно еще удалять все пробельные символы, спецсимволы, etc,
	// но об этом не сказано в задании, поэтому не реализовано
	values := strings.Split(strings.ToLower(val), "")
	sort.Strings(values)
	return strings.Join(values, "")
}
