FROM golang:1.12.13 as builder
WORKDIR /usr/src
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o aviasales .

FROM scratch
COPY --from=builder /usr/src/aviasales /go/bin/aviasales
EXPOSE 80
ENTRYPOINT ["/go/bin/aviasales"]
