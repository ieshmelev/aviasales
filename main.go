package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func buildDependencies() (*log.Logger, *api) {
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)
	cfg := newConfig()
	return logger, newAPI(cfg.APIAddr, newDict(), logger)
}

func main() {
	logger, a := buildDependencies()

	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		logger.Println("running api...")
		if err := a.run(ctx); err != nil {
			logger.Println("api runtime error", err)
			cancel()
		}
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	wg.Add(1)
	go func() {
		defer wg.Done()
		select {
		case <-sigs:
			cancel()
		case <-ctx.Done():
		}
	}()

	wg.Wait()
}
