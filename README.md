# aviasales

## preparing

### build

```bash
$ docker build --tag aviasales:latest .
```

## running

envs:

* API_ADDR, defult `:80`

```bash
$ docker run --rm -d -p 8080:80 aviasales:latest
```

handlers:

* /load
* /get

```bash
$ curl localhost:8080/load -d '["foobar", "aabb", "baba", "boofar", "test"]'
$ curl 'localhost:8080/get?word=foobar'
["foobar","boofar"]
```
