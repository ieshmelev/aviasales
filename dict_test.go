package main

import (
	"gotest.tools/assert"
	"testing"
)

func TestFailed(t *testing.T) {
	d := newDict()
	d.load([]string{"foobar", "aabb", "baba", "boofar", "test", "Abba"})

	assert.DeepEqual(t, d.getAnagrams("foobar"), []string{"foobar", "boofar"})
	assert.DeepEqual(t, d.getAnagrams("raboof"), []string{"foobar", "boofar"})
	assert.DeepEqual(t, d.getAnagrams("aBBa"), []string{"aabb", "baba", "Abba"})
	assert.DeepEqual(t, d.getAnagrams("test"), []string{"test"})
	var empty []string
	assert.DeepEqual(t, d.getAnagrams("qwerty"), empty)
}
